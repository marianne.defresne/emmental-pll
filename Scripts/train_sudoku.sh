#!/bin/bash

#symbolic Sudoku, testing the effect of k
python3 Main_PLL.py --filename E_param --seed 0 --k 10

#symbolic Sudoku, on 200 grid size
python3 Main_PLL.py --filename PLL_seed --seed 0 --k 10 --train_size 200 --epoch_max 201

#Symbolic Sudoku, many-solution dataset
python3 Main_PLL.py --train_size 1000 --One_of_Many True --path_to_data ../Data_raw/one_of_many/ --filename 1oM --seed 0

