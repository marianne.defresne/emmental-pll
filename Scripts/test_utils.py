import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from datetime import date

from utils import *
from Solver import *
from PLL import *


def val_PLL(model, val_loader, optimizer, epoch, device, filename = ""):
          
    acc_tot, PLL_tot, L_tot = 0.0, 0.0, 0
    model.eval()
    for batch_idx, (data, target) in enumerate(val_loader):
        NN_input = get_batch_input(data).to(device)  # bs, nb_var, nb_feature
        W = model(NN_input, device)

        y_true = target.type(torch.LongTensor).to(device)

        acc, PLL = val_metrics(W, y_true)
        acc_tot += acc  # correct count
        L_tot += y_true.numel()
        PLL_tot += PLL.item()
        del(W)
    
    acc_tot = acc_tot/L_tot
    print(f'Epoch: {epoch}\t Accuracy: {acc_tot:.3f}\t negPLL: {PLL_tot:.1f}')
        
    file = open("../Results/" + filename + ".txt", "a")
    file.write("\n Epoch " + str(epoch) + '- Validation: ' + str(PLL_tot) + ', ' + str(acc_tot.item()))
    file.close()
    
    return(acc_tot, PLL_tot)



def solve(W, data, quick = True, resolution = 1):

    """
    Solve the instance data based on the CFN W.
    """
            
    setUB0, margin = True, 0
    bs, grid_size = data.shape[0], int(data.shape[1]**0.5)
    L_input = [[W[b].detach().cpu().numpy(), data[b], None, False, 
                grid_size, 999999, resolution, margin, setUB0] 
               for b in range(bs)]

    L_sol = maybe_parallelize(solver, arg_list=L_input)

    if not quick:
        if len(L_sol[0])<3: #no solution with cost 0 found -> run full tb2
            #not implemented for bs > 1
            setUB0 = False
            L_input = [[W[b].detach().cpu().numpy(), data[b], None, False, 
                    grid_size, 999999, resolution, margin, setUB0] for b in range(bs)]
            L_sol = maybe_parallelize(solver, arg_list=L_input)
            
    return L_sol


def test(model, valid_data, device, quick = True, resolution = 1, filename = "", One_of_Many = False):
    
    """
    Test if the training grids are properly filled.
    Inputs: - trained model model
            - test set test_loader
            - device
            - whether to to a quick test (quick = True) or a full test. 
              If True, returns the % of correct grid for which tb2 finds a solution of cost 0
              (it is a lower bound of the true % of solved solution)
            - resolution of tb2
            - filename of the file where results are written
            
    Output: - the Hamming loss on the test set (% of correct boxes)
            - the % of grid properly filled
    """
    
    model.eval()
    test_loss, nb_solved = 0, 0
    thresh = nn.Threshold(10**(-resolution), 0)

    with torch.no_grad():        
        for full_data in enumerate(valid_data):

            if One_of_Many:
                _, dico = full_data
                data = torch.Tensor(dico["query"]).reshape(1, -1).to(device)
                targets = dico["target_set"]
                num_sample = len(valid_data)

            else:
                batch_idx, (data, target) = full_data
                num_sample = len(valid_data.dataset)
                    
            bs = data.shape[0]
            cost_fn_size = model.grid_size**2
            NN_input = get_batch_input(data).to(device)
            W = model(NN_input, device)
            W = W.view(bs, cost_fn_size, cost_fn_size, -1)

            L_sol = solve(thresh(W), data, quick = quick, resolution = resolution)

            if len(L_sol[0]) == 3: #1 solution found
                
                if One_of_Many:
                    pred = np.array(L_sol[0][0])
                    best_acc = 0
                    for target in targets:
                        target_acc = np.sum(pred == target)/cost_fn_size
                        if target_acc > best_acc:
                            best_acc = target_acc
                    test_loss += best_acc
                    if best_acc == 1:
                        nb_solved += 1
                
                else:
                    L_ytrue = [target[b] for b in range(bs)]
                    L_sudoku_solved = [Sudoku(L_sol[b][0]).sudoku_in_line
                                           for b in range (bs)]

                    nb_correct_box = torch.sum(torch.stack(L_sudoku_solved) == target)
                    hamming = nb_correct_box/cost_fn_size
                    test_loss += hamming 
                    if nb_correct_box == cost_fn_size:
                        nb_solved += 1

    if filename != "":
        file = open("../Results/"+ filename + ".txt","a")
        file.write("\n Test accuracy "+ str(test_loss/num_sample))
        file.write("% solved "+ str(nb_solved/num_sample*100) + "\n")
        file.close()
    print("Test accuracy", test_loss/num_sample)
    print("% of solved grids", nb_solved/num_sample*100)
    return(test_loss/num_sample, nb_solved/num_sample*100)
    
    
def test_1oM(model, many_data_test, device, resolution = 1, filename = None):

    grid_size, nb_var = model.grid_size, model.grid_size**2
    L = []
    for dico in many_data_test[:256]:

        data = torch.Tensor(dico["query"]).reshape(1, -1).to(device)
        targets = dico["target_set"]

        NN_input = get_batch_input(data).to(device)  
        W = model(NN_input, device)
        bs = W.shape[0]
        W = W.view(bs, nb_var, nb_var, -1)
        W = (W > 1) * 99999 #thresholding
        
        setUB = 10**-(resolution)
        all_solutions = True
        bs = data.shape[0]
        L_input = [[W[b].detach().cpu().numpy(), data[b], None, False, 
                    grid_size, 999999, resolution, 0, setUB, all_solutions] 
                   for b in range(bs)]

        L_sol = maybe_parallelize(solver, arg_list=L_input)[0]

        correct_sol = 0
        for i in range(len(L_sol)):
            if np.any(np.all(np.array(L_sol[i][1]) == targets, axis = 1)):
                correct_sol += 1
        L.append(correct_sol/len(targets))
        
    print(f"In average, {np.mean(L)*100}% of the feasible solutions are predicted per test instance.")
    if filename:
        file = open("../Results/"+ filename + ".txt","a")
        file.write(f"\nIn average, {np.mean(L)*100}% of the feasible solutions are predicted per test instance.")
        file.close()
    return np.mean(L)
