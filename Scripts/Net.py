import torch
import torch.nn as nn
import torch.nn.functional as F

def weights_init(m):

    """
    For initializing weights of linear layers (bias are put to 0).
    """
    
    if isinstance(m, nn.Linear):
        torch.nn.init.kaiming_uniform_(m.weight, mode='fan_in', nonlinearity='relu')
        torch.nn.init.zeros_(m.bias)
    

class ResBlock(nn.Module):
    
    """ 
    Residual block of 2 hidden layer for resMLP 
    Init: size of the input (the output layer as the same dimension for the sum)
          size of the 1st hidden layer
    """

    def __init__(self, input_size, hidden_size):
        super(ResBlock, self).__init__()   
        

        self.MLP = nn.Sequential(
            nn.BatchNorm1d(num_features=input_size),
            nn.ReLU(), 
            nn.Linear(input_size, hidden_size),
            nn.BatchNorm1d(num_features=hidden_size),
            nn.ReLU(), 
            nn.Linear(hidden_size, input_size)
            )

    def forward(self, x):
            
        x_out = self.MLP(x)
        x = x_out + x 
        
        return(x)
        
        
class ResMLP(nn.Module):
    
    """
    ResMLP with nblocks residual blocks of 2 hidden layers.
    Init: size of the output output size (int)
          size of the input (int)
          size of the hidden layers 
    """
    
    def __init__(self, output_size, input_size, hidden_size, nblocks = 2):
        super(ResMLP, self).__init__()   

        self.ResNet = torch.nn.Sequential()
        self.ResNet.add_module("In_layer", nn.Linear(input_size, hidden_size))
        self.ResNet.add_module("relu_1", torch.nn.ReLU())
        for k in range(nblocks):
            self.ResNet.add_module("ResBlock" + str(k), ResBlock(hidden_size, hidden_size))
        self.ResNet.add_module("Final_BN", nn.BatchNorm1d(num_features=hidden_size))
        self.ResNet.add_module("relu_n", torch.nn.ReLU())
        self.ResNet.add_module("Out_layer", nn.Linear(hidden_size, output_size))


    def forward(self, x):
        
        x = self.ResNet(x)
        
        return(x)
    


class Net(nn.Module):
    
    """
    Network composed of embedding + MLP
    Init: grid_size (int)
          hiddensize (int): number of neurons in hidden layer (suggestion: 128 or 256)
          resNet (bool). If False (default), use a regular MLP. Else, use a ResMLP
          nblocks (int): number of residual blocks. Default is 2
    """
    
    def __init__(self, grid_size, hidden_size = 128, nblocks = 2):
        super(Net, self).__init__()
    
        self.feature_size = 2 #column, line
        embedding_dim = 1
        input_size = 2*embedding_dim*self.feature_size
        self.grid_size = grid_size
        
        self.MLP = ResMLP(self.grid_size**2, input_size, hidden_size, nblocks)      
        self.MLP.apply(weights_init)  
        

    def forward(self, x, device):

        bs, nb_var, nb_ft = x.shape

        t = torch.triu_indices(nb_var,nb_var,1)
        r = x[:,t]
        rr = torch.swapaxes(r,1,2).reshape(-1,2*nb_ft)
        pred = self.MLP(rr)

        pred = pred.reshape(bs,-1,self.grid_size,self.grid_size)
        r,c = t
        out = torch.zeros(bs,nb_var,nb_var,self.grid_size,self.grid_size, device = device)
        out[:,r,c] = pred
        pred = torch.swapaxes(pred,2,3)
        out[:,c,r] = pred

        return(out)

