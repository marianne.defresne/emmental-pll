# -*- coding: utf-8 -*-

import csv
import sys
import pandas as pd
import numpy as np
import pickle
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import TensorDataset, DataLoader
import argparse

import torchvision
import hashlib

from Net import *
from utils import *
from test_utils import *
from PLL import *
from CFN import *

#taken from https://towardsdatascience.com/implementing-yann-lecuns-lenet-5-in-pytorch-5e05a0911320
class LeNet5(nn.Module):

    def __init__(self, n_classes):
        super(LeNet5, self).__init__()
        
        self.feature_extractor = nn.Sequential(            
            nn.Conv2d(in_channels=1, out_channels=6, kernel_size=5, stride=1),
            nn.Tanh(),
            nn.AvgPool2d(kernel_size=2),
            nn.Conv2d(in_channels=6, out_channels=16, kernel_size=5, stride=1),
            nn.Tanh(),
            nn.AvgPool2d(kernel_size=2),
            nn.Conv2d(in_channels=16, out_channels=120, kernel_size=5, stride=1),
            nn.Tanh()
        )

        self.classifier = nn.Sequential(
            nn.Linear(in_features=120, out_features=84),
            nn.Tanh(),
            nn.Linear(in_features=84, out_features=n_classes),
        )


    def forward(self, x):
        x = self.feature_extractor(x)
        x = torch.flatten(x, 1)
        logits = self.classifier(x)
        probs = F.softmax(logits, dim=1)

        return logits # probs


def test_mnist(network, test_loader, device):

    network.eval()
    correct, nb_non0 = 0, 0
    L = torch.zeros(10).to(device)
    with torch.no_grad():
        for data, target in test_loader:
            data = data.to(device)
            target = target.to(device)
            output = F.softmax(network(data), dim=1)
            pred = output.data.max(1, keepdim=True)[1] + 1
            L[target] += pred.eq(target.data.view_as(pred)).sum()
            if target.item() != 0:
                correct += pred.eq(target.data.view_as(pred)).sum()
                nb_non0 +=1
        print('\n MNIST test set Accuracy: {}/{} ({:.1f}%)\n'.format(correct, nb_non0,
        100. * correct / nb_non0))
    #return(L)
    

class Visual_Net(nn.Module):

    def __init__(self, grid_size, hidden_size, nblocks):
        super(Visual_Net, self).__init__()
        
        self.n_class = grid_size#+1
        self.LeNet = LeNet5(self.n_class)
        self.ResNet = Net(grid_size, hidden_size = hidden_size, nblocks = nblocks)


    def forward(self, data, batch_idx, device, img_table, label_len, mnist_set):
        
        NN_input = get_batch_input(data).to(device)
        W = self.ResNet(NN_input, device)
        
        #recognition of hints
        sample = data[0]
        sample_idx = batch_idx
        hints_idx, X = [], []
        for idx,c in enumerate(sample):
            c = int(c.item())
            if c != 0:
                #img_idx = hash(str(c)+str(idx)+str(sample_idx)) % label_len[c]
                img_idx = int(hashlib.md5((str(c)+str(idx)+str(sample_idx)).encode()).hexdigest()[:16], 16) % int(label_len[c])
                x, y = mnist_set[int(img_table[c, img_idx].item())] #y=c
                hints_idx.append(idx)
                X.append(x)

        #hints_pba = torch.ones((bs, nb_var, grid_size), device = device)*1/grid_size
        bs = data.shape[0]
        hints_pba = torch.zeros((bs, nb_var, self.n_class), device = device)
        hints_pba[:, hints_idx] = self.LeNet(torch.stack(X).to(device))
        #contains the cost predicted by LeNet for cells with a hint, otherwise 0

        return W, hints_pba
        
def test_visual(model, sudoku_test_loader, img_table_test, 
               label_len_test, mnist_test_set, device, resolution = 1, quick = False):

    model.eval()
    grid_size = model.ResNet.grid_size
    thresh = nn.Threshold(10**(-resolution), 0)
    backtrack = 100000#000

    correct, corrected =  0, 0
    with torch.no_grad():
        for batch_idx, (data, target) in enumerate(sudoku_test_loader):

            NN_input = get_batch_input(data).to(device)
            bs = data.shape[0]
            cost_fn_size = grid_size**2
            W, hints_logit = model(data = data, batch_idx = batch_idx, device = device,
                                img_table = img_table_test,label_len = label_len_test,
                                  mnist_set = mnist_test_set)
            W = thresh(W)
            W = W.view(cost_fn_size, cost_fn_size, -1).detach().cpu().numpy()

            y = target.squeeze().detach().cpu().numpy()
            d = data.squeeze().detach().cpu().numpy()
            hint = d != 0
            hints_logit = hints_logit.squeeze().detach().cpu().numpy()
            hints_logit -= np.max(hints_logit, axis = -1).reshape(-1, 1)

            #compute upper bound
            if not quick:
                oneH = torch.ones((nb_var, grid_size))
                oneH[torch.arange(nb_var), target[0].type(torch.LongTensor)-1] = 0
                Problem = make_CFN(W, 
                                   unary = np.array(oneH)*9999-hints_logit, 
                                   resolution = resolution, 
                                   backtrack = backtrack)
                pred = Problem.Solve()
                UB = pred[1] + 10**(-resolution)*10
            else:
                UB = 10**(-resolution)*10

            Problem = make_CFN(W, 
                               unary = -hints_logit, 
                               resolution = resolution, 
                               backtrack = backtrack)
            Problem.SetUB(UB)
            pred = Problem.Solve()
            if pred is not None:
                pred = np.array(pred[0])

                acc_mnist = np.sum(np.argmax(hints_logit, axis = 1)[hint]+1 == d[hint])/len(d[hint])
                acc = np.sum(y == pred)/len(y)
                correct += acc == 1
                corrected += acc == 1 and (acc_mnist < 1)
                
    num_sample = len(sudoku_test_loader.dataset)
    return correct/num_sample, corrected/num_sample
        

# Loading data: 3x3 sudoku
path_to_data = '../Data_raw/sudoku-hard/'
train_set = pd.read_csv(path_to_data + 'train.csv', names = ['x', 'y'])
valid_set = pd.read_csv(path_to_data  + 'valid.csv', names = ['x', 'y'])
test_set = pd.read_csv(path_to_data + 'test.csv', names = ['x', 'y'])

sudoku_train_loader = get_loader(train_set[:1000], batch_size = 1)
sudoku_test_loader = get_loader(test_set[:100], batch_size = 1)
sudoku_val_loader = get_loader(valid_set[:64], batch_size = 1)

hard_test_set = pd.DataFrame(columns=['x', 'y'])
for x in test_set['x']:
    if np.sum(np.array([int(c) for c in x]) != 0) == 17:
        hard_test_set = pd.concat((hard_test_set, test_set[test_set['x'] == x]))
hard_test_loader = get_loader(hard_test_set, batch_size=1)

### MNIST
mnist_train_set = torchvision.datasets.MNIST('../Data_raw', train=True, download=True,
                             transform=torchvision.transforms.Compose([
                                 torchvision.transforms.Resize((32, 32)),
                                 torchvision.transforms.ToTensor(),
                                 torchvision.transforms.Normalize(
                                 (0.1307,), (0.3081,))
                             ]))

mnist_test_set = torchvision.datasets.MNIST('../Data_raw', train=False, download=True,
                             transform=torchvision.transforms.Compose([
                                 torchvision.transforms.Resize((32, 32)),
                                 torchvision.transforms.ToTensor(),
                                 torchvision.transforms.Normalize(
                                 (0.1307,), (0.3081,))
                             ]))
mnist_test_loader = torch.utils.data.DataLoader(mnist_test_set,
                                                 batch_size=1, 
                                                 shuffle=True)
                                                 
 

argparser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
argparser.add_argument("--seed", type=int, default=0, help="manual seed") 
argparser.add_argument("--filename", type=str, default="Visual", help="filename to save results") 
args = argparser.parse_args()  

torch.manual_seed(args.seed)
filename = args.filename

                                                
if torch.cuda.is_available():  
    dev = "cuda:0" 
    print("GPU connected")
elif torch.backends.mps.is_available():
    dev = "mps"
    print("Mac MPS GPU detected")
else:  
    dev = "cpu"
    print("No GPU detected. Training on CPUs (be patient)")
device = torch.device(dev)    

grid_size = 9
nb_var = grid_size**2
hidden_size = 128
nblocks = 5
model = Visual_Net(grid_size, hidden_size = hidden_size, nblocks = nblocks)
model.to(device)

lr = 0.001
weight_decay = 0.0001
optimizer = torch.optim.Adam(model.parameters(), lr=lr, weight_decay = weight_decay)

### Table of the indices of each digit by type
img_table = -np.ones((10, 10000, 1))
for idx, (img, label) in enumerate(mnist_train_set):
    img_table[label, np.argmin(img_table[label]!=-1)] = idx
# img_table[label, idx] contains the idx-th data with label label

label_len = np.argmin(img_table!=-1, axis = 1).flatten() #number of ex of each digit


### TRAINING ###
reg_term = 1/10000*2
nb_neigh = 10 # for e-PLL (k)

if torch.cuda.is_available():
    start = torch.cuda.Event(enable_timing=True)
    end = torch.cuda.Event(enable_timing=True)
    start.record()

max_acc, best_ep = 0, 0
for epoch in range(30):
    model.train()
    print("Epoch ", epoch)
    for batch_idx, (data, target) in enumerate(sudoku_train_loader):

        optimizer.zero_grad()
        W, hints_logit = model(data, batch_idx, device, img_table, label_len, mnist_train_set)

        #computing the loss
        y_true = target.type(torch.LongTensor).to(device)
        PLL = -PLL_all(W, y_true, nb_neigh = nb_neigh, hints_logit = hints_logit)
        L1 = torch.linalg.vector_norm(W, ord = 1)
        
        loss = PLL+reg_term*L1
        loss.backward()
        optimizer.step()
 
 
    torch.save({
        'epoch': epoch,
        'model_state_dict': model.state_dict(),
        'optimizer_state_dict': optimizer.state_dict()
    }, "../Results/tb2/" + filename + "_" + str(epoch))
    acc, _ = test_visual(model, sudoku_val_loader, img_table, label_len, mnist_train_set,
                       device, resolution = 1, quick = True)
    print("% Grids solved: ", acc)
    if acc > max_acc:
        max_acc = acc
        best_ep = epoch
        
if torch.cuda.is_available():
    end.record()
    torch.cuda.synchronize()
    total_time = start.elapsed_time(end) #time in milliseconds
    print("Total training time: " + str(total_time)) 
    
    
### TEST
print("Testing")
filename = filename + "_" + str(best_ep) ### TO CHANGE ###
print(filename)
checkpoint = torch.load("../Results/tb2/"+ filename, map_location = device)
model.load_state_dict(checkpoint['model_state_dict'])
optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
epoch0 = checkpoint['epoch'] + 1

test_mnist(model.LeNet, mnist_test_loader, device)

### Table of the indices of each digit by type
img_table_test = -np.ones((10, 1500, 1))
for idx, (img, label) in enumerate(mnist_test_set):
    img_table_test[label, np.argmin(img_table_test[label]!=-1)] = idx
label_len_test = np.argmin(img_table_test!=-1, axis = 1).flatten()

correct, corrected = test_visual(model, hard_test_loader, img_table_test, 
               label_len_test, mnist_test_set, device, resolution = 1, quick = False)
               
print(f"On the hard test grids, {correct*100} % were correct, including {corrected*100} % that were corrected.")
