# -*- coding: utf-8 -*-

'''Data and part of the code adapted from https://github.com/locuslab/SATNet'''

import csv
import sys
import pandas as pd
import numpy as np
import pickle
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import TensorDataset, DataLoader
import argparse

import torchvision #in env lightning
import hashlib

from Net import *
from utils import *
from test_utils import *
from PLL import *
from CFN import *

class DigitConv(nn.Module):
    '''
    Convolutional neural network for MNIST digit recognition. From:
    https://github.com/pytorch/examples/blob/master/mnist/main.py
    '''
    def __init__(self):
        super(DigitConv, self).__init__()
        self.conv1 = nn.Conv2d(1, 20, 5, 1)
        self.conv2 = nn.Conv2d(20, 50, 5, 1)
        self.fc1 = nn.Linear(4*4*50, 500)
        self.fc2 = nn.Linear(500, 10)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 2, 2)
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, 2, 2)
        x = x.view(-1, 4*4*50)
        x = F.relu(self.fc1(x))
        x = self.fc2(x) #81, 10
        #return F.softmax(x, dim=1)[:,:9].contiguous()
        return x[:,:9].contiguous()
          

class MNISTSudokuSolver(nn.Module):
    def __init__(self, grid_size, hidden_size = 128, nblocks = 5):
        super(MNISTSudokuSolver, self).__init__()
        self.digit_convnet = DigitConv()
        self.ResNet = Net(grid_size, hidden_size = hidden_size, nblocks = nblocks)
        self.grid_size = grid_size
    
    def forward(self, x, device):
        
        digit_guess = self.digit_convnet(x.to(device))
        grid = get_batch_input(torch.ones(1, grid_size **2)).to(device)
        W = self.ResNet(grid, device)
        
        return W, digit_guess
        

def test_visual(model, mnist_test, device, resolution = 1, quick = False):

    model.eval()
    grid_size = model.ResNet.grid_size
    cost_fn_size = grid_size ** 2
    thresh = nn.Threshold(10**(-resolution), 0)
    backtrack = 100000 #000

    correct, corrected =  0, 0
    with torch.no_grad():
        for batch_idx, (X, mask, y) in enumerate(mnist_test):

            W, hints_logit = model(X, device)
            hints_logit *= mask.reshape(81, 9).to(device)

            #computing the loss
            _, target = torch.max(y.reshape(81, 9), dim = 1)
        
            W = thresh(W)
            W = W.view(cost_fn_size, cost_fn_size, -1).detach().cpu().numpy()

            y = target.squeeze().detach().cpu().numpy() + 1
            hints_logit = hints_logit.squeeze().detach().cpu().numpy()
            hints_logit -= np.max(hints_logit, axis = -1).reshape(-1, 1)

            #compute upper bound
            if not quick:
                oneH = torch.ones((nb_var, grid_size))
                oneH[torch.arange(nb_var), target[0].type(torch.LongTensor)-1] = 0
                Problem = make_CFN(W, 
                                   unary = np.array(oneH)*9999-hints_logit, 
                                   resolution = resolution, 
                                   backtrack = backtrack)
                pred = Problem.Solve()
                UB = pred[1] + 10**(-resolution)*10
            else:
                UB = 10**(-resolution)*10

            Problem = make_CFN(W, 
                               unary = -hints_logit, 
                               resolution = resolution, 
                               backtrack = backtrack)
            Problem.SetUB(UB)
            pred = Problem.Solve()
            if pred is not None:
                pred = np.array(pred[0])
                acc = np.sum(y == pred)/len(y)
                correct += acc == 1
                
    num_sample = len(mnist_test)
    return correct/num_sample
        

# Loading data: 3x3 sudoku
def process_inputs(X, Ximg, Y, boardSz):
    is_input = X.sum(dim=3, keepdim=True).expand_as(X).int().sign()

    Ximg = Ximg.flatten(start_dim=1, end_dim=2)
    Ximg = Ximg.unsqueeze(2).float()

    X      = X.view(X.size(0), -1)
    Y      = Y.view(Y.size(0), -1)
    is_input = is_input.view(is_input.size(0), -1)

    return X, Ximg, Y, is_input

path_to_data = "../Data_raw/sudoku_satnet/"
with open(path_to_data + 'features.pt', 'rb') as f:
    X_in = torch.load(f)
with open(path_to_data + 'features_img.pt', 'rb') as f:
    Ximg_in = torch.load(f)
with open(path_to_data + 'labels.pt', 'rb') as f:
    Y_in = torch.load(f)
with open(path_to_data + 'perm.pt', 'rb') as f:
    perm = torch.load(f)

boardSz = 3
X, Ximg, Y, is_input = process_inputs(X_in, Ximg_in, Y_in, boardSz)
#if args.cuda: X, Ximg, is_input, Y = X.cuda(), Ximg.cuda(), is_input.cuda(), Y.cuda()

N = X_in.size(0)
nTrain = int(N*0.9)
val_size = 64

mnist_train = TensorDataset(Ximg[:nTrain-val_size], is_input[:nTrain-val_size], Y[:nTrain-val_size])
mnist_val = TensorDataset(Ximg[nTrain-val_size:nTrain], is_input[nTrain-val_size:nTrain], 
                          Y[nTrain-val_size:nTrain])
mnist_test =  TensorDataset(Ximg[nTrain:], is_input[nTrain:], Y[nTrain:])
                                                 
 
argparser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
argparser.add_argument("--seed", type=int, default=0, help="manual seed") 
argparser.add_argument("--filename", type=str, default="Visual_satnet", help="filename to save results") 
args = argparser.parse_args()  

torch.manual_seed(args.seed)
filename = args.filename
                                                
if torch.cuda.is_available():  
    dev = "cuda:0" 
    print("GPU connected")
else:  
    dev = "cpu"
    print("No GPU detected. Training on CPUs (be patient)")
device = torch.device(dev)    

grid_size = 9
nb_var = grid_size**2
hidden_size = 128
nblocks = 5
model = MNISTSudokuSolver(grid_size, hidden_size = hidden_size, nblocks = nblocks)
model.to(device)

lr = 0.001
weight_decay = 0.0001
optimizer = torch.optim.Adam(model.parameters(), lr=lr, weight_decay = weight_decay)



### TRAINING ###
reg_term = 1/10000*2
nb_neigh = 10 # for masked PLL


if torch.cuda.is_available():
    start = torch.cuda.Event(enable_timing=True)
    end = torch.cuda.Event(enable_timing=True)
    start.record()


max_acc, best_ep = 0, 0
for epoch in range(20):
    model.train()
    print("Epoch ", epoch)
    for batch_idx, (X, mask, y) in enumerate(mnist_train):

        optimizer.zero_grad()
        W, hints_logit = model(X, device)
        hints_logit *= mask.reshape(81, 9).to(device)

        #computing the loss
        _, target = torch.max(y.reshape(81, 9), dim = 1)
        y_true = target.type(torch.LongTensor).to(device) + 1
        PLL = -PLL_all(W, y_true, nb_neigh = nb_neigh, hints_logit = hints_logit)
        L1 = torch.linalg.vector_norm(W, ord = 1)
        
        loss = PLL+reg_term*L1
        loss.backward()
        optimizer.step()
 
    torch.save({
        'epoch': epoch,
        'model_state_dict': model.state_dict(),
        'optimizer_state_dict': optimizer.state_dict()
    }, "../Results/tb2/" + filename + "_" + str(epoch))
    acc = test_visual(model, mnist_val, device, resolution = 1, quick = True)
    print("% Grids solved: ", acc)
    if acc > max_acc:
        max_acc = acc
        best_ep = epoch
        
if torch.cuda.is_available():
    end.record()
    torch.cuda.synchronize()
    total_time = start.elapsed_time(end) #time in milliseconds
    print("Total training time: " + str(total_time)) 
    
    
### TEST
print("Testing")
filename = filename + "_" + str(best_ep) ### TO CHANGE ###
print(filename)
checkpoint = torch.load("../Results/tb2/"+ filename, map_location = device)
model.load_state_dict(checkpoint['model_state_dict'])
epoch0 = checkpoint['epoch'] + 1

test_acc = test_visual(model, mnist_test, device, resolution = 1, quick = False)
print("Test grids correctly filled (corrections allowed):", test_acc*100)
test_acc = test_visual(model, mnist_test, device, resolution = 1, quick = True)
print("Test grids correctly filled (no corrections allowed):", test_acc*100)
