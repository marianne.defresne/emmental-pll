# -*- coding: utf-8 -*-
"""

"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import TensorDataset, DataLoader

from Net import*
from Sudoku import *
from test_utils import *
from PLL import *
from utils import *

def train_Hinge(model, train_loader, optimizer, epoch, device, random = False, relu = False, 
          nb_var_to_pred = "all", reg_term = 0, margin = 1, filename = "", alpha = 0):
    
    """
    Train mixing loss.
    Loss = alpha PLL + (1-alpha) Hinge
    """

    model.train() 
    cost_fn_size = model.grid_size**2
    L_loss = []
    nb_solved = 0
    resolution = 1
    setUB0 = False
    PLL_tot = 0
    nb_var = cost_fn_size

    if relu:
        thresh = nn.Threshold(10^(-resolution), 0)
        setUB0 = True
    else: 
        thresh = nn.Threshold(-1000, 0) #no thresh
    
    if setUB0:
        m = 0
    else:
        m = margin

        
    for batch_idx, (data, target) in enumerate(train_loader):
    
        optimizer.zero_grad()
        NN_input = get_batch_input(data).to(device)  # bs, nb_var, nb_feature
        W = model(NN_input, device)
        y_true = target.type(torch.LongTensor).to(device)
        
        
        ### PSEUDO-LOG LIKELIHOOD ###
        if alpha > 0:
            
            L1 = torch.linalg.vector_norm(W, ord = 1)  #L1 penalty on predicted cost
            PLL = - PLL_all(W, y_true)
            loss = alpha*(PLL + reg_term * L1)  # or mean
            PLL_tot += PLL
            
            if alpha <1: #if Hinge will be called:
                loss.backward(retain_graph=True)
            else:
                loss.backward(retain_graph=False)
                optimizer.step()
        


        ### HINGE LOSS ###
        bs = data.shape[0]
        W = W.view(bs, nb_var, nb_var, -1)
        if alpha <1:
            if relu: 
                W = F.leaky_relu(W)

            L_arg = [[thresh(W[b]).detach().cpu().numpy(), data[b], target[b], random, nb_var_to_pred, 
                      resolution, m, setUB0] 
                     for b in range(bs)]

            try:
                L_sol = maybe_parallelize(through_solver, L_arg)
                loss = np.mean([L_sol[b][1] for b in range(bs)])

                if setUB0 and loss > 0: #no solution with cost 0 found -> run full tb2
                    #not implemented for bs > 1
                    L_arg = [[thresh(W[b]).detach().cpu().numpy(), data[b], target[b], random, nb_var_to_pred, 
                          resolution, margin, False] 
                         for b in range(bs)]
                    L_sol = maybe_parallelize(through_solver, L_arg)


                grad_w = [L_sol[b][0] for b in range(bs)] #bs*81*81*81
                loss = np.mean([L_sol[b][1] for b in range(bs)])
                L_loss.append(loss)
                if loss == 0:
                    nb_solved += 1

                reg = torch.sign(W) #regularization
                grad = torch.stack(grad_w).to(device) + reg_term * reg
                grad *= (1-alpha)
                #grad *= 1/(81*40*bs)

                #update net weights
                W.backward(grad)
                optimizer.step()

            except:
                print("No solution found in train")


            if batch_idx % 100 == 0:
                #Printing/saving progress
                print('epoch {}, progress {}, loss {}'.format(epoch, 
                    int(batch_idx*bs/len(train_loader.dataset) * 100), np.mean(L_loss)))
                file = open("../Results/" + filename + ".txt","a")
                file.write("epoch "+ str(epoch) + ": "+ 
                           str(int(batch_idx*bs/len(train_loader.dataset) * 100)) 
                           + "% loss " + str(np.mean(L_loss)) +"\n")
                file.close()
                L_loss = []
                torch.save({
                            'epoch': epoch,
                            'model_state_dict': model.state_dict(),
                            'optimizer_state_dict': optimizer.state_dict()
                            }, "../Results/tb2/"+filename)
           
    if alpha <1:
        print("% of grid solved on the epoch", nb_solved/len(train_loader.dataset)*100)
    if alpha >0:
        print("Training PLL", PLL_tot.item())
    
    return(nb_solved/len(train_loader.dataset)*100)


# Loading data
path_to_data = '../Data_raw/sudoku-hard/'
x_train = pd.read_csv(path_to_data + 'train.csv', names = ['x', 'y'])

#Full dataset
train_set = pd.read_csv(path_to_data + 'train.csv', names = ['x', 'y'])
valid_set = pd.read_csv(path_to_data  + 'valid.csv', names = ['x', 'y'])
test_set = pd.read_csv(path_to_data + 'test.csv', names = ['x', 'y'])

train_size = 1000
valid_size = 64
test_size = 64

# mini-dataset
mini_train_set = train_set[:train_size]  # 1800 = 100 easy points
mini_val_set = valid_set[:valid_size]
mini_test_set = test_set[:test_size]

#Creating the dataset
batch_size = 1
train_loader = get_loader(mini_train_set, batch_size)
test_loader = get_loader(mini_test_set, batch_size = 1)
val_loader = get_loader(mini_val_set, batch_size = 1)


torch.set_printoptions(precision=2)
torch.manual_seed(0) #for reproducibility

if torch.cuda.is_available():  
    dev = "cuda:0"
    print("GPU connected")
else:  
    dev = "cpu"
device = torch.device(dev)

grid_size = 9
hidden_size = 128
nblocks = 5
resnet = True
model = Net(grid_size, hidden_size = hidden_size, nblocks = nblocks)
model.to(device)

lr = 0.001 #0.0005
weight_decay = 0.0001
optimizer = torch.optim.Adam(model.parameters(), lr=lr, weight_decay = weight_decay)
#scheduler = StepLR(optimizer, step_size = 1, gamma = 0.5)
nb_epoch = 500+1
random = False
alpha = 0


#If using the mixing, start from weights successfully trained with PLL alone 
resume_training = False
if resume_training:
	filename = "PLL_500" ### TO CHANGE ###
	checkpoint = torch.load("../Results/tb2/"+ filename)
	model.load_state_dict(checkpoint['model_state_dict'])
	optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
	epoch0 = checkpoint['epoch'] + 1
	print("Resuming training")
else:
	epoch0 = 1

filename = "Hinge"

nb_var_to_pred = 20
reg_term = 5e-05
to_test, relu = False, True
margin = 0.1


file = open("../Results/" + filename + ".txt","a")    
file.write("\n Training with the following parameters: \n ResNet: " + str(resnet) +
    "\n nblock: " + str(nblocks) + "\n hidden size: " + str(hidden_size) + "\n lr: " +
     str(lr) + "\n weight decay: " + str(weight_decay) + "\n margin: " + str(margin) +
     "\n L1: "+ str(reg_term) + "\n Train size:" + str(train_size) + "\n alpha: " + str(alpha) +
           "\n alpha += (1-alpha)/2 \n")
file.close()


#######################################################################
### Hinge approach only ###
#checkpoint and automatic increase of the number of predicted variables
loss_max = 0

L_train_loss = [0, 0]
for epoch in range (epoch0, nb_epoch): 
    
    if nb_var_to_pred != "all":
        if nb_var_to_pred >= 50:
            nb_var_to_pred = "all"
            relu = True
            to_test = True
        
    file = open("../Results/" + filename + ".txt","a")   
    file.write("\n Epoch loss:" + str(L_train_loss[-1])) 
    file.write("\n epoch "+ str(epoch) + "; number of predicted variables: "
            + str(nb_var_to_pred) + "\n")
    file.close()

    t0 = time.time()
    train_loss = train_Hinge(model, train_loader, optimizer, epoch, device, random, 
          relu, nb_var_to_pred, reg_term, margin, filename)
    L_train_loss.append(train_loss)
    
    t1 = time.time()
    print("Time for the epoch: ", t1-t0)
    print("Nomber of predicted variables", nb_var_to_pred, "Train loss", L_train_loss)
    
    #Saving
    torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict()
                }, "../Results/tb2/"+ filename + "_" + str(epoch)) 
    
    if nb_var_to_pred != "all":
        #save checkpoint when train loss is max
        if train_loss > loss_max:
        #save checkpoint when train loss start decreasing 
        #if (L_train_loss[-1] - L_train_loss[-2]<0) and (L_train_loss[-2] - L_train_loss[-3]>0):
            loss_max = train_loss
            torch.save({
                    'epoch': epoch,
                    'model_state_dict': model.state_dict(),
                    'optimizer_state_dict': optimizer.state_dict()
                    }, "../Results/tb2/"+ filename + "checkpoint") 
                   
        #if (L_train_loss[-1] - L_train_loss[-2]<0) and (L_train_loss[-2] - L_train_loss[-3]<0):
        if (L_train_loss[-1] < loss_max) and (L_train_loss[-2] < loss_max):
            #train loss decreases 2 times in a row  (loss = % grid solved)
            nb_var_to_pred += 10
            L_train_loss.append(0) #to reset decrease count
            loss_max = 0
            
            #load model just before overfitting
            checkpoint = torch.load("../Results/tb2/" + filename + "checkpoint")
            model.load_state_dict(checkpoint['model_state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
            epoch0 = checkpoint['epoch'] + 1
            
        if train_loss == 100: #everything correctly predicted
            nb_var_to_pred += 10
          
    if to_test:
        test_loss = test(model, val_loader, device, filename = filename, quick = True, resolution = 0)

